package yoga_ali_rayhan_mytodoapp;

public class Task {

    private String Desc;
    private char Status;

    public Task(String desc, char status){
        this.Desc = desc;
        this.Status = status;
    }
    public Task(){}

    public char getStatus() {
        return Status;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        this.Desc = desc;
    }

    public void setStatus(char status) {
        this.Status = status;
    }

}
